import React from 'react'
import PropTypes from 'prop-types'
import { Alert, Page, NavBar, BreadCrumb, TopBar, ErrorBoundary } from '@myntra/uikit'
import { withRootState, AppLink } from '@spectrum'
import { pathToAction } from 'redux-first-router'

import classnames from './default.module.css'

function DefaultLayout({ router, children, goto }) {
  return (
    <Page
      renderNavBar={() => (
        <NavBar
          tabIndex={1}
          currentPath={router.location.pathname}
          onNavLinkClick={({ to }) => goto(to, router.location.routesMap)}
          title="Spectrum App"
          renderLink={({ href, children, ...props }) => (
            <AppLink {...props} to={href} tabIndex={-1}>
              {children}
            </AppLink>
          )}
        >
          <NavBar.Item to="/" icon="home">
            Home
          </NavBar.Item>
        </NavBar>
      )}
      renderTopBar={() => (
        <TopBar title="Spectrum App" user={{ name: 'Anonymous' }}>
          <BreadCrumb>
            {
              router.location.pathname
                .replace(/\/$/, '')
                .split('/')
                .reduce(
                  (acc, link) => {
                    const to = acc.link + '/' + link

                    if (link === '') {
                      acc.items.push(
                        <BreadCrumb.Item key={to}>
                          <AppLink to={to}>Home</AppLink>
                        </BreadCrumb.Item>
                      )
                    } else {
                      acc.items.push(
                        <BreadCrumb.Item key={to}>
                          <AppLink to={to}>{toPascalCase(link)}</AppLink>
                        </BreadCrumb.Item>
                      )
                    }

                    return acc
                  },
                  { items: [], link: '' }
                ).items
            }
          </BreadCrumb>
        </TopBar>
      )}
    >
      <main className={classnames.container}>
        <ErrorBoundary
          renderFallback={({ error, info }) => (
            <Alert>
              {error.message}
              <br />
              <pre>{info.componentStack}</pre>
            </Alert>
          )}
        >
          {children}
        </ErrorBoundary>
      </main>
    </Page>
  )
}

DefaultLayout.propTypes = {
  router: PropTypes.any,
  goto: PropTypes.func,
  children: PropTypes.any
}

export default withRootState(state => state, { goto: (path, routes) => pathToAction(path, routes) })(DefaultLayout)

function toPascalCase(str) {
  str = str.replace(/-([a-z])?/g, (_, ch) => (ch || '').toUpperCase())

  return str[0].toUpperCase() + str.substr(1)
}
