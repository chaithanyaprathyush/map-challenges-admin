import React from 'react'

export const title = 'Home'

export default function HomePage() {
  return ((
    <section>
      <h1>Hello World</h1>

      <p>Start building awesome apps!</p>
    </section>
  ))
}
