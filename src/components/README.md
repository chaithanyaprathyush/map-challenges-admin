# Components

This directory contains reusable components. This project is aliased to `@components`.

```js
import MyComponent from '@components/my-component.jsx'
```
