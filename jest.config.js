module.exports = {
  setupFiles: ['<rootDir>/test/unit/setup-enzyme.js'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest'
  },
  collectCoverageFrom: ['src/**/*']
}
