#!/bin/env bash
if [ ! -d /myntra/$APP/node_modules ]; then 
    tar -xvf /myntra/$APP/_nm.tgz -C /myntra/$APP
fi

mkdir -p /myntra/$APP/logs
cd /myntra/$APP
node /myntra/$APP/scripts/update-meta.js > /myntra/$APP/logs/out.log
