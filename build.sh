rm -rf build node_modules yarn_modules
npm install --no-save --no-package-lock
npm run build
npm run upload:s3
tar -czf _nm.tgz node_modules
