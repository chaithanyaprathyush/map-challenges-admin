/** @type {import('@myntra/spectrum-cli-plugin').Spectrum.Project.Options} */
module.exports = {
  lintOnSave: false,
  redux: {
    useThunk: true
  }
}
