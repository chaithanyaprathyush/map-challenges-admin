const path = require('path')
const fs = require('fs')
const { findSpectrumProjectRoot } = require('@myntra/spectrum-cli-shared-utils')
const db = require('./db')
const projectDir = findSpectrumProjectRoot(process.cwd())
const buildDir = path.resolve(projectDir, 'dist')

const TIMEOUT = 60000
const spectrumDBConfig = require('spectrum-config').db;

db.init(spectrumDBConfig, getIndexURL)

function getIndexURL() {
	update(require(`${buildDir}/azIndexURL.json`))
}

async function update(payload) {
	const projectDir = findSpectrumProjectRoot(process.cwd())
	let { name } = require(path.resolve(projectDir, 'package.json'))
	name = name.replace('@myntra/spectrum-cli-app-', '')

	const entry = {
		env: process.env.NODE_ENV === 'production' ? process.env.NODE_ENV : payload.clusterDetails.clusterName,
		name: name,
		url: payload.indexURL,
		created_by: payload.clusterDetails.lastDeployedBy
	}
	try {
		const qobj = {
			sql: 'SELECT * FROM spectrum_meta WHERE name=? and env=? order by created_on ASC',
			values: [name, entry.env],
			timeout: TIMEOUT
		}
		
		const existingEntries = await db.exec(qobj)
		
		if(existingEntries.length && existingEntries[existingEntries.length-1].url ===  entry.url) return;

		const ids = []
		while (existingEntries.length > 10) {
			ids.push(existingEntries.shift().id)
		}
	
		if (ids.length) {
			const qobj = {
				sql: 'DELETE FROM spectrum_meta WHERE env=? AND id IN (' + ids.join(',') + ')',
				values: [entry.env],
				timeout: TIMEOUT
			}
			await db.exec(qobj)
		}
	} catch(err) {
		console.error('ERROR:', err)
		process.exit(1)
	}

	try {
		const qobj = {
			sql: 'INSERT INTO spectrum_meta (name, env, url, created_by, created_on) VALUES (?, ?, ?, ?, NOW())',
			values: [entry.name, entry.env, entry.url, entry.created_by],
			timeout: TIMEOUT
		}
		await db.exec(qobj)
		console.log('Deployment successfully completed')
	}  catch(err) {
		console.error('ERROR:', err)
		process.exit(1)
	}
}
