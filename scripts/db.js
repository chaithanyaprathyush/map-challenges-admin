const mysql = require('mysql');
const pick = require('lodash/pick')
const {getCredentials} = require('./utils')

const obj = {
	init: function(config, callback) {
		this.config = pick(config, 'host', 'port', 'database')
		this.isReady = false

		const { vault } = config
		console.log('[db] init host: %s port: %s database: %s', config.host, config.port, config.database)

		getCredentials(vault, (credentials) => {
			this.config.user = credentials.user
			this.config.password = credentials.secret
			this.isReady = true
			console.log('[db] ready')
			callback()
		})
	},
	exec: function(qobj) {
		if (!this.isReady) {
			return Promise.reject('DB init failed')
		}
		return new Promise((resolve, reject) => {
			const conn = mysql.createConnection(this.config)
			conn.connect(function(err) {
				if (err) {
					conn.destroy()
					return reject(err)
				}
				conn.query(qobj, function(err, results, fields) {
					if (err) {
						reject(err)
					} else {
						resolve(results)
					}
					conn.destroy()
				})
			})
		})
	}
}

module.exports = obj
