const util = require('util')
const exec = require('child_process').exec

function getCredentials(vault, callback) {
  const credentialsFormat = util.format("/usr/local/bin/keycli_linux_amd64 -service_name '%s' -credential_name '%s'",
    vault.service_name, vault.credential_name)

  exec(credentialsFormat, (error, credentials, stderr) => {
    if (error) {
      callback()
      return console.error('vault', error.toString())
    }
    callback(JSON.parse(credentials.replace('\n', '')))
  });

}

module.exports = { getCredentials }
